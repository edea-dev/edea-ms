import type { Specification } from '$lib/models/models';
import { SvelteSet } from 'svelte/reactivity';

export const userState = $state({
    specifications: [] as Specification[],
    selected_ids: new SvelteSet() as SvelteSet<Number>
});
