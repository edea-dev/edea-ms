import type { Project, User } from '$lib/models/models';

export const userState = $state({
    projects: [] as Project[],
    user: undefined as User | undefined,
});